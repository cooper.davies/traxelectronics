﻿/*
 * Copyright 2019 VizworX Inc.
 * All rights reserved
 */
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace TraxElectronics
{
    public enum DrillState
    {
        Neither = 0,
        Sliding = 1,
        Rotating = 2
    }
    public class Parser
    {
        #region Fields

        /// <summary>
        /// The directory we are parsing from;
        /// </summary>
        readonly string DirectoryFiles;

        /// <summary>
        /// The count of how many items there are
        /// </summary>
        private static int Counter = 0;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor used for generating this parser object
        /// </summary>
        /// <param name="directoryPath">The path to the directory containing the files</param>
        public Parser(string directoryPath)
        {
            FileAttributes attr = File.GetAttributes(directoryPath);
            if (!attr.HasFlag(FileAttributes.Directory))
            {
                throw new ArgumentException("Supplied path is not a directory");
            }
            DirectoryFiles = directoryPath;
        }

        #endregion

        public List<FloatExample> ParseAllList(int? NumberOfFilesToParse = null)
        {
            Log.Message("Parsing files...");
            List<FloatExample> retVal = new List<FloatExample>();
            string[] filesBefore = Directory.GetFiles(DirectoryFiles);
            string[] files = filesBefore;
            if (NumberOfFilesToParse != null)
                files = filesBefore.OrderBy(x => new System.Random().Next()).Take(NumberOfFilesToParse.Value).ToArray();
            Task<List<FloatExample>>[] tasks = new Task<List<FloatExample>>[files.Length];
            for (int i = 0; i < files.Length; i++)
            {
                int temp = i;
                //Threaded so that each file is handled in its own thread.
                tasks[temp] = Task.Factory.StartNew(
                    () =>
                    {
                        List<FloatExample> ret = ParseFile(files[temp]);
                        Log.Message(new FileInfo(files[temp]).Name + " Parsed. " + ret.Count + " cells added");
                        return ret;
                    });
            }

            //Waiting for the tasks to finish
            Task.WaitAll(tasks);
            Log.Message("Parsing finished... Collating results");
            //Now copy all arrays into one collated array (memory space efficient method)
            for (int j = 0; j < tasks.Length; j++)
            {
                retVal.AddRange(tasks[j].Result);
                tasks[j] = null;
                GC.Collect();
            }
            return retVal;
        }

        private static List<FloatExample> ParseFile(string filePath)
        {
            //Setting up output values
            List<FloatExample> retVal = new List<FloatExample>();
            using (TextFieldParser parser = new TextFieldParser(filePath))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                string[] Headers = parser.ReadFields();
                //This may be unnecessary to filter for, but I don't want to manually
                //go through every file to make sure they all have identical header
                //ordering

                //Possible room for optimization, but the increase in efficiency would be
                //negligible
                int deltaIndex = HeaderIndex(Headers, "DeltaT");

                //Using the 0 vaules because I want the raw data
                int hlIndex = HeaderIndex(Headers, "HookLoad0");

                int stIndex = HeaderIndex(Headers, "SurfaceTorque0");

                int wobIndex = HeaderIndex(Headers, "WOB0");

                int ropIndex = HeaderIndex(Headers, "ROP0");

                int srIndex = HeaderIndex(Headers, "SurfaceRotation0");

                int bhIndex = HeaderIndex(Headers, "BlockHeight0");

                int bdIndex = HeaderIndex(Headers, "BitDepth0");

                int hdIndex = HeaderIndex(Headers, "HoleDepth0");

                int bdtvdIndex = HeaderIndex(Headers, "BitDepthTVD0");

                int mpdIndex = HeaderIndex(Headers, "MotorDP0");

                int incIndex = HeaderIndex(Headers, "Inclination0");

                int cincIndex = HeaderIndex(Headers, "ContinuousInclination0");

                int gtfIndex = HeaderIndex(Headers, "GravityToolFace0");

                int mtfIndex = HeaderIndex(Headers, "MagneticToolFace0");

                int holeIndex = HeaderIndex(Headers, "MakingHole");

                int rotIndex = HeaderIndex(Headers, "DrillingRotating");

                int sliIndex = HeaderIndex(Headers, "DrillingSliding");

                
                while (!parser.EndOfData)
                {
                    //Processing row
                    string[] fields = parser.ReadFields();

                    #region Parsing

                    //DELTA
                    float delta = -1;
                    float.TryParse(fields[deltaIndex], out delta);
                    //HOOK LOAD
                    float hook = float.Parse(fields[hlIndex]);
                    //SURFACE TORQUE
                    float torque = float.Parse(fields[stIndex]);
                    //WEIGHT ON BIT
                    float wob = float.Parse(fields[wobIndex]);
                    //RATE OF PENETRATION
                    float rpen = float.Parse(fields[ropIndex]);
                    //SURFACE ROTATION
                    float srot = float.Parse(fields[srIndex]);
                    //BLOCK HEIGHT
                    float bheight = float.Parse(fields[bhIndex]);
                    //BIT DEPTH
                    float bdepth = float.Parse(fields[bdIndex]);
                    //HOLE DEPTH
                    float hdepth = float.Parse(fields[hdIndex]);
                    //TRUE VERTICAL BIT DEPTH
                    float tvbdepth = float.Parse(fields[bdtvdIndex]);
                    //MOTOR DIFFERENTIAL PRESSURE
                    float mdiff = float.Parse(fields[mpdIndex]);
                    //INCLINATION
                    float incl = float.Parse(fields[incIndex]);
                    //CONTINUOUS INCLINATION
                    float cincl = float.Parse(fields[cincIndex]);
                    //GRAVITY TOOL FACE
                    float gtface = float.Parse(fields[gtfIndex]);
                    //MAGNETIC TOOL FACE
                    float mtface = float.Parse(fields[mtfIndex]);
                    //MAKING HOLE
                    bool mhole = Convert.ToBoolean(int.Parse(fields[holeIndex]));
                    //ROTATING
                    bool rot = Convert.ToBoolean(int.Parse(fields[rotIndex]));
                    //SLIDING
                    bool slide = Convert.ToBoolean(int.Parse(fields[sliIndex]));
                    DrillState currentState = DrillState.Neither;
                    if (rot)
                        currentState = DrillState.Rotating;
                    if (slide)
                        currentState = DrillState.Sliding;

                    #endregion

                    #region Array Copying
                    float[] toAddInput = new float[]{
                        delta, hook, torque, wob, rpen, srot,
                        bheight, bdepth, hdepth, tvbdepth, mdiff,
                        incl, cincl, gtface, mtface};

                   FloatExample toAdd = new FloatExample(GetNextValue(), toAddInput, currentState);
                    //Copy to value output

                    retVal.Add(toAdd);

                    #endregion
                }
            }
            return retVal;
        }

        #region Helpers

        private static int HeaderIndex(string[] headers, string toFind)
        {
            for (int i = 0; i < headers.Length; i++)
            {
                if (headers[i].Contains(toFind))
                    return i;
            }
            return -1;
        }

        public static int GetNextValue()
        {
            return Interlocked.Increment(ref Counter);
        }

        #endregion

    }
}
